package model;

import java.util.GregorianCalendar;

import lib.DateConverter;

public class Member {
	
	private String firstName;
	private String lastName;
	private GregorianCalendar dateOfBirth;
	private String login;
	
	public Member(String firstName, String lastName, String dateOfBirth) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth=DateConverter.stringToDate(dateOfBirth);
		this.generateLogin();
	}
	
	public String getLastName(){
		return this.lastName;
	}
	
	public String getFirstName(){
		return this.firstName;
	}
	
	public GregorianCalendar getDateOfBirth(){
		return this.dateOfBirth;
	}
	
	public String getLogin(){
		return this.login;
	}
	
	private void generateLogin(){
		this.login=this.firstName.substring(0,1).concat(this.lastName);
	}
}
