package test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import model.Member;

@DisplayName("Test unitaire de la classe Member")
public class testMember {
	
	private Member member1;

	
	@BeforeEach
	void testBefore(){
		this.member1 = new Member("John","Do","19-11-1983");
	}
		
	@Test
	@DisplayName("Testlongueur : Permet de vérifier que la longueur du login est supérieur à 6")
	public void testLongueur() {
		assertTrue(this.member1.getLogin().length()>=6,"La longueur du login doit être de 6 caractères minimum");
	}
	
	
}
